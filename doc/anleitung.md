# Anleitung

## Ressourcen als Pico-8 Cart veröffentlichen

Ressourcen (Sprites, Maps, Sounds) können als Pico-8 Carts veröffentlicht werden.
Normalerweise ist diese Funktion dazu gedacht, ganze Spiele zu veröffentlichen.
Ich verwende sie hier, um Ressourcen bereit zu stellen.

In Pico-8 ist eine Art Browser integriert.
Mit ihm können veröffentlichte Spiele gesucht und geladen werden.

1.  Das Programm benötigt bestimmte Kommentare am Anfang.
        *  `--<name des programm>` - Hier ist `<name des programm>` z.B. `space-tut-01`.
        *  `--<kennung des autors>` - Hier ist `<kennung des autors>`z.B. mein Pico-8 Account `
1.  Auf der Pico-8 Konsole das Tutorume ausführen mit `run`.
1.  `F7` drücken, damit wird ein Screen-Shot vom Bildschirm gezogen. Dieser dient dann als Titelbild.
1.  Mit `ESC` gelangt man wieder auf die Pico-8 Konsole. Mit `save <cart name>.p8.png` kann man jetzt die Cartridge inklusive des Sceen-Shots speichern. Hier könnte `<cart name>`z.B. `space-tut-01` lauten.
1.  Auf der [Pico-8 Web-Seite](https://www.lexaloffle.com/pico-8.php) meldet man sich mit seinem Account an. Dieser Account entspricht der `<kennung des autors>` von oben.
1.  Aus dem Menü wählt man "Submit" aus.
1.  Jetzt muss man noch eine eindeutige ID (Kennung) für seine Cart festlegen. Man kann entweder eine ID manuell festlegen oder sich eine per Zufall generieren lassen. Ich habe meine selbst festgelegt. Z.B. `hz_space_tut_01`.
1.  Dann muss man auf "Continue" klicken. Damit gelangt man auf einen File-Auswahldialog. Hier kann man seine `.p8.png` Datei mit dem File-Browser auswählen. (Bei macOS liegen die Dateien unter `/Users/<nutzer>/Library/Application Support/pico-8/carts`. Wobei `<nutzer>`der konkrete Log-in-Name auf der Maschine ist.)
1.  Vor dem Klick auf "Upload" muss noch das Häkchen "I agree to the terms of use." gesetzt werden.
1.  Auf der nächsten Bildschirmseite kann man sich dafür oder dagegen entscheiden, jedem das Cart für die Nutzung freizugeben (Crative Commons). Ich habe mich dafür entschieden. Außerdem kann man das Cart dafür freigeben, dass es im Web-Player spielbar wird. Auch dafür habe ich mich entschieden.
1.  Zuletzt muss man "Create New Thread" klicken. Damit ist das Cart veröffentlicht.
1.  Auf der nächsten Seite kann man noch Schlagworte für die Suche vergeben. Ich habe mich für `tutorial`, `programming`, `beginner`, `space` und `game` entschieden.
1.  Darüber hinaus muss man noch eine Kategorie wählen. Hier gabe ich "PICO-8: Tutorial" gewählt.
1.  Zuletzt noch eine kleine Beschreibung und dann auf "Publish" klicken. Fertig. Jetzt können Pico-8 Nutzer mit dem eingebauten Browser "Splore" die Ressourcen zum Tutorial direkt in Pico-8 herunter laden.
