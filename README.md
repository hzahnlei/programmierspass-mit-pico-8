# Programmierspaß mit Pico-8

Programmieren lernen (für Kinder) mit der Pico-8 Fantasy Console und Lua.

Menschen lernen gerne spielerisch.
Was liegt also näher, als das Programmieren zu lernen, indem man ein Computerspiel schreibt?

In diesem Tutorium benutzt du Pico-8 als "Game Engine".
Programmiert wird Pico-8 in der Programmiersprache Lua.
Lua ist eine einfache aber leistungsfähige Programmiersprache.
Anhand von Beispielen und vorgefertigten Schablonen lernst du spielerisch und Schritt für Schritt das Programmieren.

## Voraussetzungen

Du benötigst einen Computer mit Linux, Windows 10 oder macOS.
Und du musst dir die Pico-8 Fantasy Console herunter laden ([Pico-8](https://www.lexaloffle.com/pico-8.php)).
Pico-8 ist aber nicht kostenlos.
Der Entwickler möchte dafür 14,99USD.
Dafür ist in Pico-8 aber alles integriert, was du benötigst:

*  Code-Editior
*  Sprite-Editor
*  Map-Editor
*  Sound- und Musik-Editor

Weiterhin benötigst du eine Internetverbindung.

Optional kannst du diese Tutorium auch von meinem GitLab Repository herunter laden.
Dafür benötigst du aber noch ein Programm namens Git.
Es ist aber nicht nötig.
Du kannst die Anleitung auch einfach on-line auf dieser Seite durcharbeiten.

## Los geht's

Von hier aus kannst du direkt los legen.
Im Ordner [Tutorium](/tutorium) findest du die Tutorien:

1.  [Erste Schritte Pico-8](/tutorium/01-pico-8-intro-01.md) - Hier lernst du Pico-8 und Lua kennen. Mit ein paar praktischen Übungen kannst Pico-8 und Lua noch viel besser lernen, als wenn du nur darüber lesen würdest.
1.  [Space Game](/tutorium/02-space-game-01.md) - Jetzt geht der Spaß richtig los. Hier lernst du, wie man einen "fixed position" Space Shooter baut.

## Verweise

*  [Pico-8](https://www.lexaloffle.com/pico-8.php), offizielle Web-Seite
   *  Auf der Seite des Herstellers findest du diesen [Spickzettel](https://www.lexaloffle.com/bbs/files/16585/PICO-8_Cheat-Sheet_0-9-2.png). Er enthält die wichtigsten Befehle, Tastenkürzel und eine Farbtabelle
   * Der Hersteller stellt auch eine [Anleitung](https://www.lexaloffle.com/pico8_manual.txt) zur Verfügung
*  [Lua](https://www.lua.org), offizielle Web-Seite
*  Hier gibt es einen tollen [on-line Kurs](https://mboffin.itch.io/gamedev-with-pico-8-issue1) zu Pico-8, allerdings auf Englisch
*  Auf dieser Pico-8 [Fan-Seite](https://pico-8.fandom.com/wiki/) gibt es viele nützliche Informationen. Z.B. gibt es auch eine Befehlsreferenz, allerdings auf Englisch