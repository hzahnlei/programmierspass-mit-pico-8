# Space Game Tutorium (Fortsetzung)

## Jetzt kommt Bewegung ins Spiel

Bisher war das alles ohne Bewegung und ohne Action.
Das Spiel ist sehr statisch.
Wir müssen da Bewegung rein bringen.

Wenn du dir noch mal die Sprites ansiehst, fällt dir als Sprite #2 das rotgrüne Dreieck auf.
Das ist unser Raumschiff, dass jetzt gleich durch das Weltall fliegen soll.

Wenn du die Assets geladen hast, dann gib folgendes Programm ein.

```lua
_init = function()
  ship = {x=63, y=120, speed=2, sprite=1}
end

_update60 = function()
  if (btn(0)) then
    ship.x=ship.x-ship.speed
  elseif (btn(1)) then
    ship.x=ship.x+ship.speed
  end
end

_draw = function()
  cls()
  spr(ship.sprite, ship.x, ship.y)
end
```

*  **Funktion `_init`**: Mit `ship = {x=63, y=120, speed=2, sprite=1}` wird ein Raumschiff definiert. 
        Es hat eine X- und eine Y-Koordinate.
        (63,120) bedeutet, das das Rauschiff unten, in der Mitte steht.
        Die Geschwindigkeit des Raumschiffs ist `2`.
        Es soll das Sprite #1 benutzt werden.
* **Funktion `_update60`**: Hier werden die Tasten abgefragt.
        Wenn die "Links"-Taste gedrückt wird (`btn(0)`), dann bewegt sich das Raumschiff nach Links.
        Das erreichst du, indem du von der aktuellen X-Position `speed` abziehst.
        `ship.x = ship.x - ship.speed` kann auch etwas kürzer geschrieben werden: `ship.x -= ship.speed`.<br>
        Wenn die "Rechts"-Taste gedrückt wird (`btn(1)`), dann bewegt sich das Raumschiff nach Rechts.
        Das erreichst du, indem du zu der aktuellen X-Position `speed` hinzu addierst.
        `ship.x = ship.x + ship.speed` kann auch etwas kürzer geschrieben werden: `ship.x += ship.speed`.
* **Funktion `_draw`**: Jetzt wird das aktuelle Spielgeschehen auf dem Bildschirm dargestellt.
        Zunächst wird wieder der Bildschirm gelöscht.
        Dann wird das Raumschiff gezeichnet.
        Das macht die Funktion `spr`.
        Wir nehmen dazu die Informationen aus unserem Schiff `ship.sprite`, `ship.x` und `ship.y`.

Wenn du das Programm ausführst, dann wirst du folgendes sehen:

![ship](images/02-space-game/ship-screen.png)

Am unteren Bildschirmrand befindet sich unser Raumschiff.

Benutze die Cursor-Tasten deines Computers, um es nach Links und Rechts fliegen zu lassen.

![asks](images/computer_asks.png)
Was passiert, wenn du länger auf die "Links"- oder "Rechts"-Taste drückst?

![afraid](images/computer_is_afraid.png)
Oh, nein!
Das Raumschiff verschwindet vom Bildschirm!

Bevor wir uns um das Problem kümmern, spiele doch noch etwas mit diesem Programm herum.
Was passiert, wenn du den Wert von `ship.speed` veränderst?

## Bis hierher und nicht weiter

Unser Raumschiff kann links und rechts vom Bildschirm verschwinden.
Hier ist wieder unser Programm.
Aber dieses mal habe ich für den linken Rand einen Stop eingebaut.
Tippe das Programm ab.

```lua
_init = function()
  ship = {x=63, y=120, speed=2, sprite=1}
end

_update60 = function()
  if (btn(0)) then
    ship.x -= ship.speed
    if ship.x<0 then ship.x=0 end
  elseif (btn(1)) then
    ship.x += ship.speed
    --BITTE AUSFÜLLEN--
  end
end

_draw = function()
  cls()
  spr(ship.sprite, ship.x, ship.y)
end
```

![asks](images/computer_asks.png)
Was muss hier stehen, wo `--BITTE AUSFÜLLEN--` steht?

![explains](images/computer_explains.png)
Hier mein Tipp.
Der rechte Bildschirmrand endet bei Pixel 127.
Das Raumschiff ist sieben Pixel breit.
Die rechte Endposition darf also nicht größer als 121 sein.
Der Ausdruck, der hier stehen muss, ist ähnlich zu dem: `if ship.x<0 then ship.x=0 end`.

Überlege dir selbst, was in der fehlenden Zeile stehen muss, bevor du die Lösung hier unten ansiehst.

OK, und hier die Auflösung:

```lua
_init = function()
  ship = {x=63, y=120, speed=2, sprite=1}
end

_update60 = function()
  if (btn(0)) then
    ship.x -= ship.speed
    if ship.x<0 then ship.x=0 end
  elseif (btn(1)) then
    ship.x += ship.speed
    -- So wird verhindert, dass das Raumschiff rechts raus fliegt.
    if ship.x>121 then ship.x=121 end
  end
end

_draw = function()
  cls()
  spr(ship.sprite, ship.x, ship.y)
end
```

## Speichern

OK, jetzt hast du fast schon so etwas, wie ein Computerspiel.
Das lohnt sich jetzt schon richtig diesen Stand abzuspeichern.

Wechsle wieder zur Konsole.
Hier gibts du folgendes ein:

```bash
save space1
````

Damit ist dein Spiel mit dem Namen `space1` gespeichert.

Jetzt, wo dein Spiel einen Namen hat, kannst du auch einfach durch drücken von &#8984;`-s` speichern.
Aber vorsicht damit überschreiben deine Änderungen die vorherige Version.
Wenn du ältere Versionen deines Programms aufheben möchtest, dann speichere am Besten auf der Konsole mit: `save spac2`, `save space3`usw.

Wenn du dein Spiel wieder laden möchtest, dann gibst du auf der Konsole folgendes ein:

```bash
load space1
````

Und schwupp - dein Spiel ist wieder da.

| [&#8592; voherige Übung](02-space-game-01.md) | [&#8593; zurück zur Übersicht &#8593;](../README.md) | [nächste Übung &#8594;](02-space-game-03.md) |