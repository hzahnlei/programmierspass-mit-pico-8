# Space Game Tutorium (Fortsetzung)

OK, das Raumschiff bewegt sich nach links und rechts.
Und durch das Sternenfeld sieht es so aus, als würde das Raumschiff vorwärts fliegen.
Aber das geht noch schöner.
Gleich fliegt dein Raumschiff durch einen Trümmerhaufen von Asteroiden!!!

## Kartoffelasteroiden

Tippe wieder das Programm ab.
Damit du nicht so viel Tippen musst, habe ich die neuen Teile mit `-- NEW --` gekennzeichnet.
Kopiere den Code nicht einfach.
Tippe ihn ab.
Duch genaues Beobachten und Lesen lernst du am meisten.

In Lua beginne Kommentare übrigens mit zwei Bindestrichen, oder Minuszeichen `--`.
In einem Kommentar kannst du für dich selbst und andere Beschreiben, was der Code macht oder wie man ihn benutzen soll.

```lua
_init = function()
  init_ship();
  init_starfield()
  -- NEW --
  init_asteroid_belt()
end

init_ship = function()
  ship = {x=63, y=120, speed=2, sprite=1}
end

init_starfield = function()
  starfield = {stars={}, star_cols={1,2,5,6,7,12}, warp_factor=3}
  for i = 1, #starfield.star_cols do
    for j = 1, 10 do
      local s={
        x=rnd(128),
        y=rnd(128),
        z=i,
        c=starfield.star_cols[i]
      }
      add(starfield.stars,s)
    end 
  end
end

-- NEW --
init_asteroid_belt = function()
  asteroid_belt={}
  for i = 1, 10 do
    local ast={}
    init_asteroid(ast)
    add(asteroid_belt, ast)
  end
end

-- NEW --
init_asteroid = function(ast)
  ast.x = rnd(128)
  ast.y = -32
  local ast_type=rnd(4)
  ast.speed = 1 + (ast_type/2.0)
  ast.sprite = 7 + ast_type
end

_update60 = function()
  update_ship()
  update_starfield()
  -- NEW --
  update_asteroid_belt()
end

update_ship = function()
  if (btn(0)) then
    ship.x -= ship.speed
    if ship.x<0 then ship.x=0 end
  elseif (btn(1)) then
    ship.x += ship.speed
    if ship.x>121 then ship.x=121 end
  end
end

update_starfield = function()
  for s in all(starfield.stars) do
    s.y += s.z * starfield.warp_factor / 10
    if s.y > 128 then
      s.y = 0
      s.x = rnd(128)
    end
  end
end

-- NEW --
update_asteroid_belt = function()
  for ast in all(asteroid_belt) do
    ast.y += ast.speed
    if ast.y >= 128 then
      init_asteroid(ast)
    end
  end
end

_draw = function()
  cls()
  draw_starfield()

  -- NEW --
  draw_asteroid_belt()

  draw_ship()
end

draw_starfield = function()
  for s in all(starfield.stars) do
    pset(s.x,s.y,s.c)
  end
end

draw_ship = function()
  spr(ship.sprite, ship.x, ship.y)
end

-- NEW --
draw_asteroid_belt = function()
  for ast in all(asteroid_belt) do
    spr(ast.sprite,ast.x,ast.y)
  end
end
```

![explains](images/computer_explains.png)
Und hier wieder die Erklärung, wie das funktioniert.

### init_asteroid_belt

```lua
init_asteroid_belt = function()
  asteroid_belt={}
  for i = 1, 10 do
    local ast={}
    init_asteroid(ast)
    add(asteroid_belt, ast)
  end
end
```

Der Name `asteroid_belt` bezeichnet die, zunächst leere `{}`, Liste von Asteroiden.

In der Schleife `for i = 1, 10 do` werden zehn Asteroiden erzeugt.
Probiere gerne aus, wie das Spiel mit mehr oder weniger Asteroiden aussieht.
Aber bedenke:

*  Zu wenige Asteroiden und das Spiel ist zu einfach und langweilig.
*  Zu viele Asteroiden und das Spiel ist zu schwer und frustrierend.

Der Name `ast` bezeichnet einen Asteroiden.
Dieser wird jetzt durch die Funktion `init_asteroid` initialisiert.

Mit `add(asteroid_belt, ast)` wird der Asteroid dem Asteroidengürtel hinzu gefügt.

### init_asteroid

```lua
init_asteroid = function(ast)
  ast.x = rnd(128)
  ast.y = -32
  local ast_size=rnd(4)
  ast.speed = 1 + (ast_size/2.0)
  ast.sprite = 7 + ast_size
end
```

Diese Funktion initialisiert einen einzigen Asteroiden.
Der Asteroid wird als Parameter `ast` an die Funktion übergeben.
Hier enthält der Asteroid eine zufällige X-Koordinate `ast.x = rnd(128)`.
Seine Y-Position wird oben, außerhalb des Bildschirms gewählt `ast.y = -32`.
Schließlich soll es so aussehen, als flöge der Asteroid in das Bild hinein.
Er soll nicht einfach so plötzlich im Bild auftauchen.

In den Assets seht ihr vier verschiedene Asteroidenbildchen.
Mit `local ast_type=rnd(4)` wird eine von vier verschieden Größen ausgewählt.
Aus dem Grund sind die Assets auch streng vong Groß nach Klein sortiert.

Mit diesem Ausdruck `ast.speed = 1 + (ast_size/2.0)` bekommt jeder Asteroid eine Geschwindigkeit.
Kleinere Asteroiden sind schneller, größere langsamer.
Wenn du den Ausdruck veränderst, dann kannst du die Geschwindigkeit beeinflussen.
Aber passe auf, sind die Asteroiden zu schnell, dann hat der Spieler keine Chance.
Sind sie zu langsam, dann fühlt sich der Spieler nicht genug herausgefordert.

Das erste Asteroiden-Sprite ist das Sprite #7.
Mit `ast.sprite = 7 + ast_size` wird jedem Asteroiden ein Sprite-Bildchen zugeordnet, das seiner Größe und Geschwindigkeit entspricht.

### update_asteroid_belt

```lua
update_asteroid_belt = function()
  for ast in all(asteroid_belt) do
    ast.y += ast.speed
    if ast.y >= 128 then
      init_asteroid(ast)
    end
  end
end
```

Hier werden die Asteroiden von Oben nach Unten bewegt.
`asteroid_belt` ist eine Liste von Asteroiden.
Mit `for ast in all(asteroid_belt) do` gehen wir jeden einzelnen Asteroiden durch.

`ast.y += ast.speed` bewegt einen Asteroiden mit seiner individuellen Geschwindigeit vorwärts.

Jetzt müssen wir wieder aufpassen.
Was passiert, wenn ein Asteroid unten angekommen ist?
Das prüfen wir mit `if ast.y >= 128 then`.
In dem Fall wird der Asteroid recycled.
Wir rufen `init_asteroid(ast)` auf.
Damit bekommt der Asteroid wieder eine zufällige Position am oberen Bildschirmrand.
Auch seine Größe und Geschwindigkeit werden wieder zufällig bestimmt.

### draw_asteroid_belt

```lua
draw_asteroid_belt = function()
  for ast in all(asteroid_belt) do
    spr(ast.sprite,ast.x,ast.y)
  end
end
```

Das Zeichnen der Asteroiden ist nicht sehr spektakulär.
Wir gehen wieder durch die Liste aller Asteroiden `for ast in all(asteroid_belt) do`.
Dann wird jeder einzelne Asteroid gezeichnet `spr(ast.sprite,ast.x,ast.y)`.

## Mehr Explosionen

![bored](images/computer_is_bored.png)
Oh man, ist das öde.
Da passiert ja garnichts, wenn man mit den Asteroiden kollidiert.
Ich will mehr Explosionen!

Was jetzt kommt nennt sich auf englisch Collision Detection.
Du must jetzt herausfinden, ob das Raumschiff mit einem Asteroiden kollidiert.
Dazu sieh dir folgende Abbildung an.

<img src="images/02-space-game/collision.svg" width="25%" height="25%"/>

Stell dir vor, ein Asteroid befindet sich an Position (10,10).
Und an Position (14,14) befindet sich dein Raumschiff.
Die einfachste Collision Detection ist, einfach so zu tun, als seien alle Sprites Quadrate von acht auf acht Pixeln.
Wenn sich jetzt diese Quadrate (Bounding Box) überlagern, dann ist das eine Kollision.
Das ist nicht sehr genau.
Das Raumschiff ist ja eigentlich ein Dreieck.
Und die kleineren Asteroiden füllen ihr Quadrat gar nicht voll aus.
Aber fürs Erste ist das genau genug.

Für die Collision Detecten prüfen wir also nur, ob die obere linke Ecke unseres Raumschiffs innerhalb eines Asteroidenquadrats liegt.

```lua
_init = function()
  init_ship();
  init_starfield()
  init_asteroid_belt()
end

init_ship = function()
  ship = {x=63, y=120, speed=2, sprite=1}
end

init_starfield = function()
  starfield = {stars={}, star_cols={1,2,5,6,7,12}, warp_factor=3}
  for i = 1, #starfield.star_cols do
    for j = 1, 10 do
      local s={
        x=rnd(128),
        y=rnd(128),
        z=i,
        c=starfield.star_cols[i]
      }
      add(starfield.stars,s)
    end 
  end
end

init_asteroid_belt = function()
  asteroid_belt={}
  for i = 1, 10 do
    local ast={}
    init_asteroid(ast)
    add(asteroid_belt, ast)
  end
end

init_asteroid = function(ast)
  ast.x = rnd(128)
  ast.y = -32
  local ast_type=rnd(4)
  ast.speed = 1 + (ast_type/2.0)
  ast.sprite = 7 + ast_type
end

_update60 = function()
  update_ship()
  update_starfield()
  update_asteroid_belt()
  -- NEW --
  detect_collision()
end

update_ship = function()
  if (btn(0)) then
    ship.x -= ship.speed
    if ship.x<0 then ship.x=0 end
  elseif (btn(1)) then
    ship.x += ship.speed
    if ship.x>121 then ship.x=121 end
  end
end

update_starfield = function()
  for s in all(starfield.stars) do
    s.y += s.z * starfield.warp_factor / 10
    if s.y > 128 then
      s.y = 0
      s.x = rnd(128)
    end
  end
end

update_asteroid_belt = function()
  for ast in all(asteroid_belt) do
    ast.y += ast.speed
    if ast.y >= 128 then
      init_asteroid(ast)
    end
  end
end

-- NEW --
detect_collision = function()
  for ast in all(asteroid_belt) do
    if (ship.x     < ast.x + 6) and
       (ship.x + 6 > ast.x)     and
       (ship.y     < ast.y + 7) and
       (ship.y + 7 > ast.y) then
        ship.sprite=18
        break
    end
  end
end

_draw = function()
  cls()
  draw_starfield()
  draw_asteroid_belt()
  draw_ship()
end

draw_starfield = function()
  for s in all(starfield.stars) do
    pset(s.x,s.y,s.c)
  end
end

draw_ship = function()
  spr(ship.sprite, ship.x, ship.y)
end

draw_asteroid_belt = function()
  for ast in all(asteroid_belt) do
    spr(ast.sprite,ast.x,ast.y)
  end
end
```

![explains](images/computer_explains.png)
Und so funktioniert es.

```lua
detect_collision = function()
  for ast in all(asteroid_belt) do
    if (ship.x     < ast.x + 6) and
       (ship.x + 6 > ast.x)     and
       (ship.y     < ast.y + 7) and
       (ship.y + 7 > ast.y) then
        ship.sprite=18
        break
    end
  end
end
```

Wir müssen für jeden einzelnen Asteroiden prüfen, ob unser Raumschiff damit kollidiert ist.
Das machen wir wieder mit der `for`-Schleife `for ast in all(asteroid_belt) do`.
Mit dem `if` prüfen wir, ob die X-Koordinate der Raumschiffs innerhalb der Breite des Asteroiden liegt.
**Beachte**: Es werden nur sieben Pixel geprüft, weil unser Raumschiff auch nur sieben Pixel breit ist.
Und wir prüfen auch im `if`-Ausdruck, ob die Y-Koordinate des Raumschiffs innerhalb der Höhe des Asteroiden liegt.
Hier zählen wir aber alle acht Pixel.
Und warum steht da dann `7`?
Das ist, weil Computer oft bei null anfangen zu zählen.

Wenn das Raumschiff getroffen wurde, dann setzen wir einfach sein Sprite um.
Das Sprite #18 ist eine fette Explosion.
Nicht zu übersehen.
So können wir schnell erkennen, ob unsere Kollisionserkennung funktioniert.

Wenn das Raumschiff getroffen wurde dann brechen wir die Schleife mit `break` ab.
Warum machen wir das?
Es reicht, wenn das Raumschiff von einem Asteroiden getroffen wurde.
Wir müssen dann nicht auch noch die anderen Asteroiden prüfen.
Diese Rechenzeit können wir uns sparen.

![demanding](images/computer_is_determined.png)
Das nennst du eine Explosion?
Dass ich nicht lache.
Streng dich mal mehr an!

OK, in der nächsten Übung wirst du eine heftige Explosion programmieren.

| [&#8592; voherige Übung](02-space-game-03.md) | [&#8593; zurück zur Übersicht &#8593;](../README.md) | [nächste Übung &#8594;](02-space-game-05.md) |