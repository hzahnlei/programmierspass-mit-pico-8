# Erste Schritte mit Lua und Pico-8 (Fortsetzung)

## Der Pico-8 Bildschirm

Im vorherigen Beispiel hast du mit der Funktion `circfill` experimentiert.
Wenn du die Funktionsargumente verändert hast, dann hast du gesehen, dass der Kreis an einer anderen Stelle auf dem Bildschirm erschienen ist.

Ich habe die Worte "X-Koordinate" und "Y-Koordinate" erwähnt.
Hier erfährst du, was das ist.

Der Bildschirm von Pico-8 ist nicht sehr groß.
Er misst nur 128 auf 128 Pixel.
(Ein Pixel ist ein Bildpunkt auf dem Bildschirm.)

Die folgende Abbildung soll den Bildschirm von Pico-8 darstellen.
In der linken, oberen Ecke findest du den Bildpunkt mit den Koordinaten X=0 und Y=0.
Du kannst das kürzer schreiben: (0,0).
Da Computer meißtens bei 0 anfangen zu zählen, ist die rechte, untere Koordinate nicht (128,128), sondern (127, 127).
Wenn du einen Kreis in der Bildschirmmitte malen möchtest, dann nimmst du die Koordinaten (63,63).

<img src="images/01-pico-8-intro/coordinates.svg" width="25%" height="25%"/>

Jetzt tippe doch mal folgendes Programm ab und schaue, was auf dem Bildschirm passiert.

**Tipp**: Wenn dir das zuviel Tipperei ist, kannst du das Programm auch in die Zwischenablage kopieren und im Editor einsetzten.

```lua
cls()
circfill(10,10,8,0)
circfill(15,15,8,1)
circfill(20,20,8,2)
circfill(25,25,8,3)
circfill(30,30,8,4)
circfill(35,35,8,5)
circfill(40,40,8,6)
circfill(45,45,8,7)
circfill(50,50,8,8)
circfill(55,55,8,9)
circfill(60,60,8,10)
circfill(65,65,8,11)
circfill(70,70,8,12)
circfill(75,75,8,13)
circfill(80,80,8,14)
circfill(85,85,8,15)
```

![excited](images/computer_is_excited.png)
Sieht das nicht super aus! Und die vielen Farben erst.

Spiele doch mal mit dem Programm herum.
Ändere Koordinaten, Radien und Farben und sieh, was passiert.

![explains](images/computer_explains.png)
Ach ja die Farben.
Welche Farbe ist die Nummer `3` noch mal?
Welche Nummer hat die Gelb?

Hier ist die Tabelle der Farben, wie sie ursprünglich bei Pico-8 von Hersteller vorgegeben sind.
Beachte, dass der Computer wieder bei der Null damit Anfängt die Farben aufzuzählen.

<img src="images/01-pico-8-intro/color_table.svg" width="15%" height="15%"/>

Das Programm oben malt ein tolles Bild.
Und es ist einfach, damit zu experimentieren.
Es ist aber auch sehr lang.
So möchtest du lieber kein ganzes Spiel programmieren müssen.

![determined](images/computer_is_determined.png)
**Es wird Zeit mehr Lua zu lernen!**

Tippe das folgende Programm ab.
Diesmal solltest du es nich über die Zwischenablage kopieren.
Beim Abtippen lernst du mehr, weil du aufmerksamer bist und dir das Programm, das du abtippst, zwangsmäßig genauer ansiehst.

```lua
cls()
for i=0,15 do
  local pos=10+(5*i)
  circfill(pos,pos,8,i)
end
```

![asks](images/computer_asks.png)
Hast du das Programm laufen lassen?
Was ist diesmal auf dem Bildschirm passiert?

![explains](images/computer_explains.png)
Aha!
Genau das selbe, wie bei dem langen Programm.
Du siehst also, wenn man mehr Funktionen von Lua kennt, dann kann man mit kürzeren, mächtigeren Programmen, den selben Effekt erziehlen, wie mit einem einfacheren, aber dafür längeren Programm.

Wie Arbeitet das Programm?
Zunächst wird wieder mit `cls` der Bildschirm geleert.
`for` ist eine sogenannte **Schleife**.
Alles zwischen `do` und `end` wird 16 mal wiederholt.
Dabei ist `i` die sogenannte **Laufvariable**.
Sie verändert sich mit jedem Durchlauf.
Beim ersten Durchlauf gilt `i=0`.
Beim zweiten Durchlauf gilt `i=1` usw.

Die folgende Abbildung soll dir das verdeutlichen.

![cross_balls](images/01-pico-8-intro/cross_balls_index_var.png)

`local` ist (wie `for`, `do`und `end`) ein sogenanntes Schlüsselwort.
Das sorgt dafür, dass die Variable `pos` nur innerhalb der Schleife zu sehen ist.
`pos` ist eine **lokale** Variable.
`pos` repräsentiert die Position, an der ein Kreis gezeichnet werden soll.
Wir fangen mit 10 Pixeln Abstand vom Rand an.
Damit die Kreise nicht so eng beieinander stehen multiplizieren wir `i` mit fünf.
Das heißt, dass die Mittelpunkte der Kreise immer um fünf Pixel zum vorherigen Kreis verschoben ist.

Mit `circfill(pos,pos,8,i)` zeichnest du schließlich die Kreise.
Weil du die selbe Pixelanzahl `pos` sowohl für die X-, als auch für die Y-Koordinate benutzt, werden die Kreise auf einer Diagonalen angeordnet.

Alle Kreise haben den selben Radius von acht.

Als Farbcode benutzt du einfach `i` so, wie es ist.
Erinnere dich an die Farbtabelle.
Es gibt nur die Farben 0, 1, 2, ..., 15.

![explains](images/computer_explains.png)
Um noch besser zu verstehen, was ein Programm macht, ist es hilfreich, wenn du es mal auf einem Blatt Papier selbst durchrechnest.

Fülle die Tabelle aus, indem du die Werte selbst berechnest.
Wenn du das tust, dann spielst du im Kopf das durch, was ein Computer auch macht.
Damit verstehst du, wie ein Computer funktioniert.

| `i` | `pos=10+(5*i)`         | `circfill(pos,pos,8,i)` |
|-----|------------------------|-------------------------|
| 0   | 10+(5&sdot;0)=10+0=10  | `circfill(10,10,8,0)`   |
| 1   | 10+(5&sdot;1)=10+5=15  | `circfill(15,15,8,1)`   |
| 2   | 10+(5&sdot;2)=10+10=20 | `circfill(20,20,8,2)`   |
| 3 | | |
| 4 | | |
| 5 | | |
| 6 | | |
| 7 | | |
| 8 | | |
| 9 | | |
| 10 | | |
| 11 | | |
| 12 | | |
| 13 | | |
| 14 | | |
| 15 | | |

![explains](images/computer_explains.png)
Du kannst mit dem Programm weiter experimentieren.
Was passiert, wenn du `pos` nur für die X-Koorinate nutzt und die Y-Koordinate fest auf z.B. 63 setzt: `circfill(pos,pos,8,i)`?
Und was passiert, wenn du die X-Koorinate fest auf z.B. 63 setzt und `pos` nur für die Y-Koordinate verwendets?

## Zufallszahlen

Eine wichtige Funktion für Computerspiele ist die Funktion `rnd`.
`rnd` berechnet Zufallszahlen.
Der Zufall ist für Computerspiele sehr wichtig.
Z.B. kannst du den Zufall dazu benutzten, um bei Tetris das nächste Pusselteil auszuwürfeln, das oben zum Bildschirm herein fliegen soll.
Man kann auch Levels automatisch generieren.
Damit sie nicht so steril und langweilig rüber kommen, nutzt man den Zufall.

Vereinfacht gestagt, produziert `rnd(100)` eine Zufallszahl zwischen 0 und 100.
Das kannst du z.B. zum zufälligen Positionieren von Objekten auf dem Bildschirm nutzen.
Probiere das mal aus.

```lua
cls()
for i=0,1000 do
  local x=10+rnd(100)
  local y=10+rnd(100)
  local radius=2+rnd(6)
  local color=rnd(15)
  circfill(x,y,radius,color)
end
```

![excited](images/computer_is_excited.png)
Sieht das nicht wie Konfetti aus?
Führe das Programm mehrmals aus.
Du wirst beobachten, dass das Ergebnis bei jedem Durchlauf anders aussieht.
Was für ein Zufall!

| [&#8592; voherige Übung](01-pico-8-intro-01.md) | [&#8593; zurück zur Übersicht &#8593;](../README.md) | [nächste Übung &#8594;](02-space-game-01.md) |
