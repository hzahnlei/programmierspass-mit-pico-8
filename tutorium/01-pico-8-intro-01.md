# Erste Schritte mit Lua und Pico-8

Lua ist eine einfache aber leistungsfähige Programmiersprache.
Entwickelt wurde sie bereits 1993 an einer brasilianischen Universität.
In Brasilien spricht man Portugisisch.
Lua ist portugisisch und heißt Mond.
(Wenn dich das interessiert, dann kannst du hier mehr über Lua erfahren: https://de.wikipedia.org/wiki/Lua.)

Da Lua eine sehr "kleine" Sprache ist, wird sie gerne in Game-Engines eingebaut.
Dann kann man sehr einfach Spiele mit Lua entwickeln.
Und genau so haben es die Entwicler von Pico-8 auch gemacht.

Starte jetzt Pico-8.
Auf einem Mac klickst du dazu im Launchpad auf das Pico-8 Icon.
Das Launchpad findest du in der Regel unten links um Dock.
Wenn du Pico-8 vor kurzem benutzt hast, dann ist sein Symbol oft noch im Dock zu finden.

Hier siehst du den macOS Desktop mit dem Symbol für das Launchpad:

![launchpad](images/01-pico-8-intro/macos-launchpad.png)

So sieht das Symbol von Pico-8 aus:

![pico8icon](images/01-pico-8-intro/pico-8-icon.png)

Klicke darauf und Pico-8 wird gestartet.

Nachdem Pico-8 gestartet ist, befindest du dich in der sogenannten Konsole.
Hier kannst du verschiedene Befehle eingeben.
Aber dazu später mehr.
So sieht die Konsole aus:

![konsole](images/01-pico-8-intro/pico-8-start-screen.png)

Drücke jetzt die `ESC`-Taste, oben links auf deiner Tastatur.
Mit dieser Taste kannst du zwischen der Konsole und dem Editor hin und her wechseln.
Im **Editor** schreibst du programme, zeichnest deine Spielfiguren und entwirfst die Sounds.
In der **Konsole** kannst du z.B. dein Spiel speichern oder starten.

So sieht der Editor aus:

![editor](images/01-pico-8-intro/code-editor.png)

Tippe folgendes Programm ab.
Ich erkläre dir dann gleich, was es macht.

```lua
print("hello world")
```

Im Editor sieht das dann so aus:

![edit hello world](images/01-pico-8-intro/edit-hello-world.png)

Jetzt wieder mit `ESC` auf die Konsole wechseln.
Gib dort `run` ein und drücke die `ENTER`-Taste.
(Die `ENTER`-Taste ist oft mit diesem Pfeil beschriftet: &#9166;.)
Du solltest jetzt folgendes auf dem Bildschirm sehen:

![run hello world](images/01-pico-8-intro/run-hello-world.png)

![congratulates](images/computer_explains.png)
Gratuliere!
Du hast gerade dein erstes Programm geschrieben!

Übrigens, das Programm, das du gerade geschrieben hast ist weltberühmt.
Es heißt "Hello, World!".
Wie eine Programmiersprage aussieht und benutzt wird, wird gerne am Hello-World-Beispiel gezeigt.
Es gibt sogar eine Web-Seite, auf der unglaublich viele Hello-Worlds für alle möglichen Programmiersprachen gesammelt sind: [The Hello World Collection](http://helloworldcollection.de).
Dort gibt es auch ein [Beispiel für Lua](http://helloworldcollection.de/#Lua).

**Was passiert da?**
`print` ist eine sogenannte **Funktion**.
Die `print`-Funtion wird dazu benutzt, Texte auf dem Bildschirm auszugeben.
Indem du `print` schreibst und runde Klammern dahinter setzt, "rufst" du die Funktion auf.
Der Compuer führt die Funktion aus.
Hier bedeutet das, dass ein Text auf dem Bildschirm erscheint.

Schau dir die folgende Abbildung an.
Eine Funktion, die du aufrufen willst hat einen Namen.
In runden Klammern kommen die sogenanntem "Funktionsargumente".

<img src="images/01-pico-8-intro/function_application.svg" width="50%" height="50%"/>

Aber es gibt in Lua und Pico-8 noch viele andere nützliche Funktionen.
Die Funktion `cls` z.B. löscht den Bildschirm.
Damit beseitigst du das Chaos vom Bildschirm und bekommst eine schöne, saubere Ausgabe.

Probiere es aus.
Tippe das Programm ab und starte es.
Inzwischen weißt du, wie das geht.

```lua
cls()
print("1")
print("2")
print("3")
```

![asks](images/computer_asks.png)
Schau dir den Ausdruck `cls()` genau an.
Weleches Argument bekommt diese Funktion beim Aufruf?

![explains](images/computer_explains.png)
Richtig!
Kein Argument!
Verschiedene Funktionen benötigen unterschiedlich viele Argumente.
`cls` benötigt kein Argument.
`print` benötigt ein Argument.

Jetzt lass uns noch eine Funktion kennen lernen, die vier Argumente benötigt.

```lua
cls()
circfill(63,63,20,14)
```

![asks](images/computer_asks.png)
Was hat die Funktion `circfill` gemacht?

![explains](images/computer_explains.png)
Sie hat einen roten Kreis in der Mitte des Bildschirms gezeichnet.

Finde doch mal heraus, was passiert, wenn du die Argumente änderst.
Wofür steht die erste 63?
Wofür die zweite?
Was bedeutet die 20?
Und was die 14?

**Hier ein Tipp**: Die ersten drei Argumente sind Zahlen im Bereich von 0 bis 127.
Das vierte Argument sind Zahlen im Bereich von 0 bis 15.
Probiere auch aus, was passiert, wenn du negative Zahlen einsetzt.
Es geht auch ganz bestimmt nichts kaputt.

Und hier noch die Auflösung.
Die Funktion `circfill` ist so definiert:

```lua
circfill(x,y,r,c)
```

Keine Angst, wenn du noch nicht alles verstehts.
Das wird später alles noch erklärt.

Dabei stehen die Buchstaben für folgendes:

| Parameter | Bedeutung |
|-----------|-----------|
| `x` | Die X-Koordinate (horizontal) auf dem Bildschirm. Bei negativen Zahlen rutscht der Kreis links aus dem Bildschirm heraus. Bei Zahlen, die größer als 127 sind wird der Kreis rechts außerhalb vom Bildschirm positioniert. |
| `y` | Die Y-Koordinate (vertikal) auf dem Bildschirm. Bei negativen Zahlen rutscht der Kreis oben aus dem Bildschirm heraus. Bei Zahlen, die größer als 127 sind wird der Kreis unten außerhalb vom Bildschirm positioniert. |
| `r` | Der Radius, wie groß der Kreis sein soll. |
| `c` | Der Farbcode. Es gibt die Farben 0 bis 15. Welche Farben das genau sind, zeige ich dir später. |

So, das war die erste Übung.
Mach mit der nächsten Weiter, wann immer du Lust dazu hast.

| &#8592; voherige Übung | [&#8593; zurück zur Übersicht &#8593;](../README.md) | [nächste Übung &#8594;](01-pico-8-intro-02.md) |
