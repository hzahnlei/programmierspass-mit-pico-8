# Space Game Tutorium (Fortsetzung)

## Explodierende Asteroiden

```lua
--space-tut-01
--hzahnlei

_init = function()
  init_ship();
  init_starfield()
  init_asteroid_belt()
end

init_ship = function()
  ship = {x=63, y=120, speed=2, sprite=1, hit=false, expl_sprite=16, shoots=0}
end

init_starfield = function()
  starfield = {stars={}, star_cols={1,2,5,6,7,12}, warp_factor=3}
  for i = 1, #starfield.star_cols do
    for j = 1, 10 do
      local s={
        x=rnd(128),
        y=rnd(128),
        z=i,
        c=starfield.star_cols[i]
      }
      add(starfield.stars,s)
    end 
  end
end

init_asteroid_belt = function()
  asteroid_belt={}
  for i = 1, 10 do
    local ast={}
    init_asteroid(ast)
    add(asteroid_belt, ast)
  end
end

init_asteroid = function(ast)
  ast.x = rnd(128)
  ast.y = -32
  local ast_type=rnd(4)
  ast.speed = 1 + (ast_type/2.0)
  ast.sprite = 7 + ast_type
  -- NEW --
  ast.hit=false
  ast.expl_sprite=16
end

_update60 = function()
  update_ship()
  update_starfield()
  update_asteroid_belt()
  if not ship.hit then
    detect_collision()
  end
  if ship.shoots > 0 then
    update_shot()
  end
end

update_ship = function()
  if ship.hit then
    ship.expl_sprite+=0.1
    if ship.expl_sprite>=22 then
      _init()
    end
  else
    if btn(0) then
      ship.x -= ship.speed
      if ship.x<0 then
        ship.x=0
      end
    elseif btn(1) then
      ship.x += ship.speed
      if ship.x>121 then
        ship.x=121
      end
    end
    if btn(5) and (ship.shoots==0) then
      ship.shoots=1
      sfx(2)
    end
  end
end

update_starfield = function()
  for s in all(starfield.stars) do
    s.y += s.z * starfield.warp_factor / 10
    if s.y > 128 then
      s.y = 0
      s.x = rnd(128)
    end
  end
end

update_asteroid_belt = function()
  for ast in all(asteroid_belt) do
    -- CHANGEND --
    if ast.hit then
      ast.expl_sprite+=0.1
      if ast.expl_sprite>=22 then
        init_asteroid(ast)
      end
    else
      ast.y += ast.speed
      if ast.y >= 128 then
        init_asteroid(ast)
      end
    end
  end
end

detect_collision = function()
  for ast in all(asteroid_belt) do
    if (ship.x     < ast.x + 6) and
       (ship.x + 6 > ast.x)     and
       (ship.y     < ast.y + 7) and
       (ship.y + 7 > ast.y) then
        ship.hit=true
        sfx(1)
        break
    end
  end
end

update_shot = function()
  ship.shoots+=1
  if ship.shoots<5 then
    detect_asteroid_hit();
  elseif ship.shoots>50 then
    ship.shoots=0
  end
end

detect_asteroid_hit = function()
  for ast in all(asteroid_belt) do
    if 
      -- NEW --
      (not ast.hit) and
      (ship.x+2 >= ast.x) and (ship.x+2 <= ast.x+7) then
        -- CHNAGED -- ast.sprite=18
        ast.hit = true
        sfx(1)
        -- REMOVED -- break
    end
  end
end

_draw = function()
  cls()
  draw_starfield()
  draw_asteroid_belt()
  draw_ship()
end

draw_starfield = function()
  for s in all(starfield.stars) do
    pset(s.x,s.y,s.c)
  end
end

draw_ship = function()
  if ship.hit then
    spr(ship.expl_sprite, ship.x, ship.y)
  else
    spr(ship.sprite, ship.x, ship.y)
  end
  if ship.shoots>0 and ship.shoots < 5 then
    line(ship.x+3, ship.y, ship.x+3, 0, 11)
  end
end

draw_asteroid_belt = function()
  for ast in all(asteroid_belt) do
    if ast.hit then
      spr(ast.expl_sprite,ast.x,ast.y)
    else
      spr(ast.sprite,ast.x,ast.y)
    end
  end
end
```

```lua
ast.hit=false
ast.expl_sprite=16
```

In `init_asteroid_belt` bekommt jeder Asteroid zusätzliche Informationen.
Dies ist analog zum Raumschiff.

*  `ast.hit`
   *  `ast.hit=false` bedeutet, dass ein Asteroid nicht getroffen wurde.
      Er verhält sich also "normal" so wie bisher auch.
      Es wird ein Asteroidenbildchen angezeigt und der Asteroid fällt von oben nach unten.
   *  `ast.hit=true` bedeutet dagegen, dass der Asteroid geroffen wurde.
      Es soll in dem Fall die Explosionsanimation angezeigt werden.
      Außerdem sieht es super aus, wenn die Explosion nicht einfach weiter fliegt, sondern an der Stelle verharrt, wo der Ateroid getroffen wurde.
      Das sieht dann so aus, als ob der Laserstrahl den Asteroiden voll aus den Socken gehauen hat.
*  `ast.expl_sprite` verweist wieder auf die Explosionsanimation, genau, wie beim Raumschiff.

```lua
if ast.hit then
  ast.expl_sprite+=0.1
  if ast.expl_sprite>=22 then
    init_asteroid(ast)
  end
```

In `update_asteroid_belt` unterscheiden wir zwischen getroffenem und nicht getroffenem Asteroiden.

Wenn der Asteroid getroffen wurde, dann wird die Explosionsanimation abgespielt.
Aber, der Asteroid wird nicht mehr bewegt.

Wenn der Asteroid dagegen nicht getroffen wurde, dann läuft das Programm weiter, wie bisher.

```lua
if (not ast.hit) and
   (ship.x+2 >= ast.x) and
   (ship.x+2 <= ast.x+7) then
        ast.hit = true
        sfx(1)
end
```

In `detect_asteroid_hit` erfolgt jetzt eine Aktion, wenn ein Asteroid getroffen wird nur noch, wenn der Asteroid nicht bereits getroffen wurde.
Damit wird verhindert, dass das Explosionsgeräusch mehrfach gestartet wird.

Bei einem Treffer, wird der Asteroid als getroffen markiert `ast.hit = true`.
Dann wird der Sound-Effekt gestartet `sfx(1)`.

Den `break`-Befehlt haben wir entfern.
Damit ist es jetzt möglich, mehrere Asteroiden mit einem Schuss zu zerstören.
Warum auch nicht?
Ist halt ein super starker Laser.
Dafür dauert es halt immer ein bisschen, bis er wieder aufgeladen ist.

| [&#8592; voherige Übung](02-space-game-06.md) | [&#8593; zurück zur Übersicht &#8593;](../README.md) | nächste Übung &#8594; |