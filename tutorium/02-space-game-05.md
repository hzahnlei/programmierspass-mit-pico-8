# Space Game Tutorium (Fortsetzung)

## Rums

Und hier wieder ein neue Version.
Diesmal mit Explosion.
Bist du schon gespannt?

Ich habe die Stellen im Code, die ich geändert habe, mit `-- MODIFIED --`  oder `-- NEW --` markiert.
Dann musst du nicht alles abtippen.

```lua
_init = function()
  init_ship();
  init_starfield()
  init_asteroid_belt()
end

init_ship = function()
  -- MODIFIED --
  ship = {x=63, y=120, speed=2, sprite=1, hit=false, expl_sprite=16}
end

init_starfield = function()
  starfield = {stars={}, star_cols={1,2,5,6,7,12}, warp_factor=3}
  for i = 1, #starfield.star_cols do
    for j = 1, 10 do
      local s={
        x=rnd(128),
        y=rnd(128),
        z=i,
        c=starfield.star_cols[i]
      }
      add(starfield.stars,s)
    end 
  end
end

init_asteroid_belt = function()
  asteroid_belt={}
  for i = 1, 10 do
    local ast={}
    init_asteroid(ast)
    add(asteroid_belt, ast)
  end
end

init_asteroid = function(ast)
  ast.x = rnd(128)
  ast.y = -32
  local ast_type=rnd(4)
  ast.speed = 1 + (ast_type/2.0)
  ast.sprite = 7 + ast_type
end

_update60 = function()
  update_ship()
  update_starfield()
  update_asteroid_belt()
  -- NEW --
  if not ship.hit then
    detect_collision()
  end
end

-- MODIFIED --
update_ship = function()
  if ship.hit then
    ship.expl_sprite+=0.1
    if ship.expl_sprite>=22 then
      _init()
    end
  else
    if (btn(0)) then
      ship.x -= ship.speed
      if ship.x<0 then
        ship.x=0
      end
    elseif (btn(1)) then
      ship.x += ship.speed
      if ship.x>121 then
        ship.x=121
      end
    end
  end
end

update_starfield = function()
  for s in all(starfield.stars) do
    s.y += s.z * starfield.warp_factor / 10
    if s.y > 128 then
      s.y = 0
      s.x = rnd(128)
    end
  end
end

update_asteroid_belt = function()
  for ast in all(asteroid_belt) do
    ast.y += ast.speed
    if ast.y >= 128 then
      init_asteroid(ast)
    end
  end
end

detect_collision = function()
  for ast in all(asteroid_belt) do
    if (ship.x     < ast.x + 6) and
       (ship.x + 6 > ast.x)     and
       (ship.y     < ast.y + 7) and
       (ship.y + 7 > ast.y) then
        -- MODIFIED --
        ship.hit=true
        sfx(1)
        break
    end
  end
end

_draw = function()
  cls()
  draw_starfield()
  draw_asteroid_belt()
  draw_ship()
end

draw_starfield = function()
  for s in all(starfield.stars) do
    pset(s.x,s.y,s.c)
  end
end

-- MODIFIED --
draw_ship = function()
  if ship.hit then
    spr(ship.expl_sprite, ship.x, ship.y)
  else
    spr(ship.sprite, ship.x, ship.y)
  end
end

draw_asteroid_belt = function()
  for ast in all(asteroid_belt) do
    spr(ast.sprite,ast.x,ast.y)
  end
end
```

### init_ship

```lua
init_ship = function()
  ship = {x=63, y=120, speed=2, sprite=1, hit=false, expl_sprite=16}
end
```

Fällt dir der Unterschied auf?
Das Raumshiff trägt jetzt zwei neue Informationen:

*  `hit` zeigt an, ob das Raumschiff getroffen wurde. Am Anfang ist das Raumschiff natürlich noch unversehrt, daher hat `ship.hit` den Wert `false`. Das bedeutet, _"Nein das Schiff ist nicht getroffen!"_.
*  `expl_sprite` ist das Sprite/Bildchen, das die Explosion darstellt.

### _update60

```lua
_update60 = function()
  update_ship()
  update_starfield()
  update_asteroid_belt()
  if not ship.hit then
    detect_collision()
  end
end
```

Hier wird geprüft, ob das Raumschiff bereits getroffen wurde.
Wenn das Raumschiff getroffen wurde, dann brauchen wir die Kollisionserkennung nicht mehr aufrufen.
Diese Rechenzeit können wir uns sparen.

Von der Rechenzeit abgesehen ist das `if not ship.hit then detect_collision() end` noch aus einem anderen Grund wichtig.
Wenn wir `detect_collision()` einfach immer, ohne Bedingung, aufrufen, dann könnte es passieren, dass das Rumschiff wieder kolidiert und explodiert, obwohl es doch ohnehin gerade am explodieren ist.
Dann würde sich der Effekt störend wiederholen!

### update_ship

```lua
update_ship = function()
  if ship.hit then
    ship.expl_sprite+=0.1
    if ship.expl_sprite>=22 then
      _init()
    end
  else
    if (btn(0)) then
      ship.x -= ship.speed
      if ship.x<0 then
        ship.x=0
      end
    elseif (btn(1)) then
      ship.x += ship.speed
      if ship.x>121 then
        ship.x=121
      end
    end
  end
end
```

Die Funktion wurde stark verändert.
Der Teil im `else` Block ist genau wir zuvor.
Aber die Struktur der Funktion hat sich geändert.
Es gibt jetzt eine `if`-`else`-Konstruktion um die Abfrage der Steuerknöpfe.

```lua
update_ship = function()
  if ship.hit then
    -- NEW--
  else
    -- OLD --
  end
end
```

![asks](images/computer_asks.png)
Die Steuerknöpfe werden nur abgefragt, wenn das Schiff nicht getroffen ist.
Hast du eine Idee warum?

![explains](images/computer_explains.png)
Das ist, damit sich die Explosion nicht bewegt, wenn der Spieler auf die Tasten drückt.
Während der Explosion ist der normale Spielverlauf ausgesetzt.

Wenn das Schiff getroffen ist, dann passiert folgendes:

```lua
ship.expl_sprite+=0.1
if ship.expl_sprite>=22 then
  _init()
end
```

Der Wert für das Explisions-Sprite wird um einen winzigen Wert hochgezählt.
Die Zahl `0.1` würde in deutscher Sprache als `0,1` geschrieben.
In Programmiersprachen verwendet man anstelle des Kommas aber den Punkt.

Die Sprites #16 bis #21 bilden eine Animationssequenz.
Sobald das Sprite #22 erreicht wird, ist die Explosion zuende.
Es wird wieder `_init` aufgerufen und das Spiel startet von vorne.

Die Explosion besteht also aus 6 verschiedenen Bildchen.
Das Bild wird 60 mal pro Sekunde aktualisiert.
Wenn du `ship.expl_sprite+=1` anstelle `ship.expl_sprite+=0.1` schreiben würdest, wäre die Explision sehr schnell vorbei.
** Probiere das doch einmal aus. **
Indem wir nur 0,1 addieren, strecken wir die Animation auf ziemlich genau eine Sekunde.

Dabei benutzen wir einen Trick.
Computer können mit Kommazahlen rechnen.
Für viele Dinge benutzen sie aber ganze Zahlen.
Zum Beispiel die Auswahl der Sprites erfolgt mit ganzen Zahlen.
Der Computer bildet die Kommazahlen bei der Spriteauswahl deshalb automatisch auf ganze Zahle ab.
Es werden einfach die Nachkommastellen ignoriert.

<img src="images/02-space-game/float_to_sprite_mapping.svg" width="50%" height="50%"/>

### detect_collision

```lua
detect_collision = function()
  for ast in all(asteroid_belt) do
    if (ship.x     < ast.x + 6) and
       (ship.x + 6 > ast.x)     and
       (ship.y     < ast.y + 7) and
       (ship.y + 7 > ast.y) then
        -- MODIFIED --
        ship.hit=true
        sfx(1)
        break
    end
  end
end
```

Die Prüfung ist wieder wie zuvor.
Aber die Aktion im Faller einer Kollision ist jetzt eine andere.
Wir setzen nicht einfach das Sprite um.
Statt dessen setzen wir `ship.hit=true` und markieren damit, dass das Schiff getroffen wurde.
Mit `sfx(1)` spielen wir ein Explosionsgräusch ab.
`sfx` ist eine neue Funktion, die du bisher noch nicht kanntest.
Ich will hier auch nicht zu sehr auf die Sounds eingehen.
Nur so viel: Ich habe den Soundeffekt #1 vorbereitet.
Er ist Teil der Assets, die du runter geladen hast.

### draw_ship

```lua
draw_ship = function()
  if ship.hit then
    spr(ship.expl_sprite, ship.x, ship.y)
  else
    spr(ship.sprite, ship.x, ship.y)
  end
end
```

Hier werden jetzt zwei Fälle unterschieden:

1.  Wenn das Raumschiff getroffen wurde, dann wird die Explosionsanimation gezeigt.
        Die Variable `ship.expl_sprite` wird ja weiter oben im Programm langsam hochgezählt.
        Damit wird ein Explosionsbildchen nach dem anderen angezeigt.
1.  Wenn das Raumschiff nicht getroffen wurde, dann zeigen wir das ganz normale Raumschiffbild.

| [&#8592; voherige Übung](02-space-game-05.md) | [&#8593; zurück zur Übersicht &#8593;](../README.md) | [nächste Übung &#8594;](02-space-game-06.md) |