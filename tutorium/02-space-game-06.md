# Space Game Tutorium (Fortsetzung)

## Dauerfeuer

Dein Rumschiff hat nur die Möglichkeit den Asteroiden auszuweichen.
Wie währe es, wenn es zu seiner Verteidigung schießen könnte?

```lua
--space-tut-01
--hzahnlei

_init = function()
  init_ship();
  init_starfield()
  init_asteroid_belt()
end

init_ship = function()
  ship = {x=63, y=120, speed=2, sprite=1, hit=false, expl_sprite=16
      -- NEW --
      , shoots=0}
end

init_starfield = function()
  starfield = {stars={}, star_cols={1,2,5,6,7,12}, warp_factor=3}
  for i = 1, #starfield.star_cols do
    for j = 1, 10 do
      local s={
        x=rnd(128),
        y=rnd(128),
        z=i,
        c=starfield.star_cols[i]
      }
      add(starfield.stars,s)
    end 
  end
end

init_asteroid_belt = function()
  asteroid_belt={}
  for i = 1, 10 do
    local ast={}
    init_asteroid(ast)
    add(asteroid_belt, ast)
  end
end

init_asteroid = function(ast)
  ast.x = rnd(128)
  ast.y = -32
  local ast_type=rnd(4)
  ast.speed = 1 + (ast_type/2.0)
  ast.sprite = 7 + ast_type
end

_update60 = function()
  update_ship()
  update_starfield()
  update_asteroid_belt()
  if not ship.hit then
    detect_collision()
  end
end

update_ship = function()
  if ship.hit then
    ship.expl_sprite+=0.1
    if ship.expl_sprite>=22 then
      _init()
    end
  else
    if btn(0) then
      ship.x -= ship.speed
      if ship.x<0 then
        ship.x=0
      end
    elseif btn(1) then
      ship.x += ship.speed
      if ship.x>121 then
        ship.x=121
      end
    end
    -- NEW --
    if btn(5) and (ship.shoots==0) then
      ship.shoots=1
      sfx(2)
    end
  end
end

update_starfield = function()
  for s in all(starfield.stars) do
    s.y += s.z * starfield.warp_factor / 10
    if s.y > 128 then
      s.y = 0
      s.x = rnd(128)
    end
  end
end

update_asteroid_belt = function()
  for ast in all(asteroid_belt) do
    ast.y += ast.speed
    if ast.y >= 128 then
      init_asteroid(ast)
    end
  end
end

detect_collision = function()
  for ast in all(asteroid_belt) do
    if (ship.x     < ast.x + 6) and
       (ship.x + 6 > ast.x)     and
       (ship.y     < ast.y + 7) and
       (ship.y + 7 > ast.y) then
        ship.hit=true
        sfx(1)
        break
    end
  end
end

_draw = function()
  cls()
  draw_starfield()
  draw_asteroid_belt()
  draw_ship()
end

draw_starfield = function()
  for s in all(starfield.stars) do
    pset(s.x,s.y,s.c)
  end
end

draw_ship = function()
  if ship.hit then
    spr(ship.expl_sprite, ship.x, ship.y)
  else
    spr(ship.sprite, ship.x, ship.y)
  end
  -- NEW --
  if ship.shoots>0 then
    line(ship.x+3, ship.y, ship.x+3, 0, 11)
    ship.shoots=0
  end
end

draw_asteroid_belt = function()
  for ast in all(asteroid_belt) do
    spr(ast.sprite,ast.x,ast.y)
  end
end
```

Starte das Programm und drücke auf dem Gamepad die Feuertaste.
Auf der Tastatur ist das die `X`-Taste.

Und?
Super Effekt.
Aber, es werden noch keine Asteroiden zerstört.
Außerdem kannst du Dauerfeuer machen, indem du einfach die Feuertaste gedrückt hälst.
Das macht das Spiel zu einfach.
Zwischen den Schüssen muss es eine Zwangspause geben, sonst fehlt dem Spieler die Herausforderung.

Ich erkläre dir jetzt die Erweiterungen zur vorangegangenen Version.

```lua
ship = {x=63, y=120, speed=2, sprite=1, hit=false, expl_sprite=16, shoots=0}
```

Das Schiff hat jetzt die zusätzliche Information `shoots`.
`shoots=0` bedeutet, das Schiff schießt nicht.
`shoots>0` bedeutet dagegen, dass das Schiff schießt.

```lua
if btn(5) and (ship.shoots==0) then
  ship.shoots=1
  sfx(2)
end
```

Hiermit wird geprüft, ob der Spieler die "Feuerntaste" `btn(5)` gedrückt hat.
Wenn ja, dann wird der Schuss gestartet `ship.shoots=1`.
Außerdem wird ein weiterer Soundeffekt gestartet `sfx(2)`.
Der Schuss wird aber nur gestartet, wenn das Raumschiff nich ohnehin schon shießt `and (ship.shoots==0)`.

```lua
if ship.shoots>0 then
  line(ship.x+3, ship.y, ship.x+3, 0, 11)
  ship.shoots=0
end
```

Hier wird eine grüne Linie gezeichtent.
Die Linie geht von der Spitze des Raumschiffs aus `(ship.x+3, ship.y)`.
Sie endet am oberen Bildschirmrand `(ship.x+3, 0)`.
Außerdem wird jetzt wieder die Information vermerkt, dass das Schiff fertig ist mit der Ballerei `ship.shoots=0`.
Auch hier gilt wieder: Die ganze Aktion wird nur ausgehührt, wenn das Schiff schießt `if ship.shoots>0 then`.

## Einzelschuss

Das Dauerfeuer wäre zu einfach für den Spiele.
Du muss das irgendwie eingrenzen.
Darum habe ich für die Information `ship.shoots` nicht einfach die Werte `true` und `false` gewählt.
`ship.shoots` repräsentiert einen Zahlenwert.
Den können wir hochzählen und damit erreichen, dass die Kanone blockiert und nicht zu schnell schießt.

Für deine Spieler musst du natürlich eine glaubwürdige Geschichte erfinden, warum die Kanon nicht im Dauerfeuer schießen kann.
Zum Beispiel könnte es sein, dass sie erst wieder mit Energie aufgeladen werden muss.

_Das gibt viele neue Möglichkeiten.
Du könntest das Spiel vielleicht so programmieren, dass das Schießen den Energievorrat des Schiffes erschöpft.
Es könnte Power-Ups geben, die das Schiff einfangen muss, um den Energievorrat wieder aufzufüllen._

```lua
--space-tut-01
--hzahnlei

_init = function()
  init_ship();
  init_starfield()
  init_asteroid_belt()
end

init_ship = function()
  ship = {x=63, y=120, speed=2, sprite=1, hit=false, expl_sprite=16, shoots=0}
end

init_starfield = function()
  starfield = {stars={}, star_cols={1,2,5,6,7,12}, warp_factor=3}
  for i = 1, #starfield.star_cols do
    for j = 1, 10 do
      local s={
        x=rnd(128),
        y=rnd(128),
        z=i,
        c=starfield.star_cols[i]
      }
      add(starfield.stars,s)
    end 
  end
end

init_asteroid_belt = function()
  asteroid_belt={}
  for i = 1, 10 do
    local ast={}
    init_asteroid(ast)
    add(asteroid_belt, ast)
  end
end

init_asteroid = function(ast)
  ast.x = rnd(128)
  ast.y = -32
  local ast_type=rnd(4)
  ast.speed = 1 + (ast_type/2.0)
  ast.sprite = 7 + ast_type
end

_update60 = function()
  update_ship()
  update_starfield()
  update_asteroid_belt()
  if not ship.hit then
    detect_collision()
  end
  -- NEW --
  if ship.shoots > 0 then
    update_shot()
  end
end

update_ship = function()
  if ship.hit then
    ship.expl_sprite+=0.1
    if ship.expl_sprite>=22 then
      _init()
    end
  else
    if btn(0) then
      ship.x -= ship.speed
      if ship.x<0 then
        ship.x=0
      end
    elseif btn(1) then
      ship.x += ship.speed
      if ship.x>121 then
        ship.x=121
      end
    end
    if btn(5) and (ship.shoots==0) then
      ship.shoots=1
      sfx(2)
    end
  end
end

update_starfield = function()
  for s in all(starfield.stars) do
    s.y += s.z * starfield.warp_factor / 10
    if s.y > 128 then
      s.y = 0
      s.x = rnd(128)
    end
  end
end

update_asteroid_belt = function()
  for ast in all(asteroid_belt) do
    ast.y += ast.speed
    if ast.y >= 128 then
      init_asteroid(ast)
    end
  end
end

detect_collision = function()
  for ast in all(asteroid_belt) do
    if (ship.x     < ast.x + 6) and
       (ship.x + 6 > ast.x)     and
       (ship.y     < ast.y + 7) and
       (ship.y + 7 > ast.y) then
        ship.hit=true
        sfx(1)
        break
    end
  end
end

-- NEW --
update_shot = function()
  ship.shoots+=1
  if ship.shoots>50 then
    ship.shoots=0
  end
end

_draw = function()
  cls()
  draw_starfield()
  draw_asteroid_belt()
  draw_ship()
end

draw_starfield = function()
  for s in all(starfield.stars) do
    pset(s.x,s.y,s.c)
  end
end

draw_ship = function()
  if ship.hit then
    spr(ship.expl_sprite, ship.x, ship.y)
  else
    spr(ship.sprite, ship.x, ship.y)
  end
  if ship.shoots>0 
  -- NEW --
    and ship.shoots < 5
  then
    line(ship.x+3, ship.y, ship.x+3, 0, 11)
  end
end

draw_asteroid_belt = function()
  for ast in all(asteroid_belt) do
    spr(ast.sprite,ast.x,ast.y)
  end
end
```

```lua
if ship.shoots > 0 then
  update_shot()
end
```

Hier habe ich das jetzt so gelöst, dass "der Schuss" in einer eigenen Funktion `update_shot` aktualisiert wird.

```lua
update_shot = function()
  ship.shoots+=1
  if ship.shoots>50 then
    ship.shoots=0
  end
end
```

Hier wird einfach die `ship.shoots` Information pro Frame um eins erhöht `ship.shoots+=1`.

Nach 50 Frames wird der `ship.shoots` auf null gesetzt.
Jetzt kann das Schiff wieder feuern.
Du erinnerst dich, das Bild wird 60 mal in der Sekunde aufbebaut.
Wenn wir bis 50 zählen, dann kann der Spiele ungefähr einen Schuss pro Sekunde feuern.

Das ist eine Frage der Balance.
Du musst experimentell heraus finden, ob 50 wirklich die richtige Zahl ist.
Vielleicht macht es mehr Spaß, wenn man schneller schießen kann.
Vielleicht macht es aber mehr Spaß, wenn man weniger Schüsse abgeben kann.

Die siehst jetzt hoffentlich auch, wie du mit verschiedenen Zahlen das Spiel schneller oder langsamer, schwieriger oder einfacher machen kannst.

```lua
if ship.shoots>0 and ship.shoots < 5 then
  line(ship.x+3, ship.y, ship.x+3, 0, 11)
end
````

Obwohl `ship.shoots` bis 50 hochgezählt wird, wird der Laserstrahl nur kurz angezeigt.
Er soll mehr wie ein Blitz wirken und nicht die ganze zeit "an sein".
Das erreichen wir durch `ship.shoots>0 and ship.shoots < 5`.

## Treffer

Noch hat der Strahl keine Auswirkung.
Die Asteroiden reagieren nicht, wenn sie getroffen werden.

![determined](images/computer_is_determined.png)
Jetzt hau mal rein!
Ich will sehen, dass die Asteroiden explodieren.

```lua
--space-tut-01
--hzahnlei

_init = function()
  init_ship();
  init_starfield()
  init_asteroid_belt()
end

init_ship = function()
  ship = {x=63, y=120, speed=2, sprite=1, hit=false, expl_sprite=16, shoots=0}
end

init_starfield = function()
  starfield = {stars={}, star_cols={1,2,5,6,7,12}, warp_factor=3}
  for i = 1, #starfield.star_cols do
    for j = 1, 10 do
      local s={
        x=rnd(128),
        y=rnd(128),
        z=i,
        c=starfield.star_cols[i]
      }
      add(starfield.stars,s)
    end 
  end
end

init_asteroid_belt = function()
  asteroid_belt={}
  for i = 1, 10 do
    local ast={}
    init_asteroid(ast)
    add(asteroid_belt, ast)
  end
end

init_asteroid = function(ast)
  ast.x = rnd(128)
  ast.y = -32
  local ast_type=rnd(4)
  ast.speed = 1 + (ast_type/2.0)
  ast.sprite = 7 + ast_type
end

_update60 = function()
  update_ship()
  update_starfield()
  update_asteroid_belt()
  if not ship.hit then
    detect_collision()
  end
  if ship.shoots > 0 then
    update_shot()
  end
end

update_ship = function()
  if ship.hit then
    ship.expl_sprite+=0.1
    if ship.expl_sprite>=22 then
      _init()
    end
  else
    if btn(0) then
      ship.x -= ship.speed
      if ship.x<0 then
        ship.x=0
      end
    elseif btn(1) then
      ship.x += ship.speed
      if ship.x>121 then
        ship.x=121
      end
    end
    if btn(5) and (ship.shoots==0) then
      ship.shoots=1
      sfx(2)
    end
  end
end

update_starfield = function()
  for s in all(starfield.stars) do
    s.y += s.z * starfield.warp_factor / 10
    if s.y > 128 then
      s.y = 0
      s.x = rnd(128)
    end
  end
end

update_asteroid_belt = function()
  for ast in all(asteroid_belt) do
    ast.y += ast.speed
    if ast.y >= 128 then
      init_asteroid(ast)
    end
  end
end

detect_collision = function()
  for ast in all(asteroid_belt) do
    if (ship.x     < ast.x + 6) and
       (ship.x + 6 > ast.x)     and
       (ship.y     < ast.y + 7) and
       (ship.y + 7 > ast.y) then
        ship.hit=true
        sfx(1)
        break
    end
  end
end

update_shot = function()
  ship.shoots+=1
  -- NEW --
  if ship.shoots<5 then
    detect_asteroid_hit();
  elseif ship.shoots>50 then
    ship.shoots=0
  end
end

-- NEW --
detect_asteroid_hit = function()
  for ast in all(asteroid_belt) do
    if (ship.x+2 >= ast.x) and (ship.x+2 <= ast.x+7) then
        ast.sprite=18
        sfx(1)
        break
    end
  end
end

_draw = function()
  cls()
  draw_starfield()
  draw_asteroid_belt()
  draw_ship()
end

draw_starfield = function()
  for s in all(starfield.stars) do
    pset(s.x,s.y,s.c)
  end
end

draw_ship = function()
  if ship.hit then
    spr(ship.expl_sprite, ship.x, ship.y)
  else
    spr(ship.sprite, ship.x, ship.y)
  end
  if ship.shoots>0 and ship.shoots < 5 then
    line(ship.x+3, ship.y, ship.x+3, 0, 11)
  end
end

draw_asteroid_belt = function()
  for ast in all(asteroid_belt) do
    spr(ast.sprite,ast.x,ast.y)
  end
end
```

```lua
update_shot = function()
  ship.shoots+=1
  if ship.shoots<5 then
    detect_asteroid_hit();
  elseif ship.shoots>50 then
    ship.shoots=0
  end
end
```

Solange `ship.shoots` kleiner als fünf ist, prüfen wir, ob der Strahl mit einem Asteroiden kollidiert ist.
Ansonsten läuft der Zähler weiter bis 50.
Aber ab fünf ist der Strahl nicht mehr "aktiv".

```lua
detect_asteroid_hit = function()
  for ast in all(asteroid_belt) do
    if (ship.x+2 >= ast.x) and (ship.x+2 <= ast.x+7) then
        ast.sprite=18
        sfx(1)
        break
    end
  end
end
```

Die Funktion `detect_asteroid_hit` prüft, ob ein Asteroid von dem Strahl getroffen wurde `(ship.x+2 >= ast.x) and (ship.x+2 <= ast.x+7)`.
Wenn das so ist, dann setzen wir das Sprite des Asteroiden auf ein Explosionsbildchen und starten den Explosionssound.

Probiere es aus und starte das Programm.

Das ist natürlich noch nicht das finale Ergebnis.
Aber jetzt wissen wir wenigstens, dass die Lösung prinzipiell funktioniert.
Folgendes fehlt noch:

*  Die Explosion muss animiert werden.
*  Ein explodierender Asteroid darf keinen Schaden mehr an deinem Schiff anrichten.

| [&#8592; voherige Übung](02-space-game-05.md) | [&#8593; zurück zur Übersicht &#8593;](../README.md) | [nächste Übung &#8594;](02-space-game-07.md) |