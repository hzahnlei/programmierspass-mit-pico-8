# Space Game Tutorium

## Assets/Ressourcen

Zunächst brauchst du ein paar "Ressourcen" oder auch "Assets".
Ressourcen oder auch Assets sind Raumschiffe und andere Charaktäre, die man über den Bildschirm steuern kann.
Aber auch Sound-Effekte und Musik sind Ressourcen.
Ich habe einige davon vorbereitet.
Die must du jetzt herunterladen.

Wechsele jetzt auf die Pico-8 Konsole, wenn du noch nicht bereits dort bist.
(Drücke hierfür die `ESC`-Taste, um vom Editor auf die Konsole zu gelangen.)
Dort gibst du den Befehl `splore` ein.
Splore ist ein eingebauter Browser.
Mit ihm kannst du fertige Spiele aber auch Tutorien etc. finden.

Drücke nun die "Cursor nach rechts" Taste, bis der Begriff "Search" erscheint.
Dies sieht ungefähr so aus:

![Splore Such-Fenster](images/02-space-game/splore-search.png "Splore search")

Gib nun den Suchbegriff `tutorial` ein.
Hier wähle `hz_space_tut_01` des Nutzers `hzahnleiter` aus und drücke dann die `ENTER`-Taste.

Jetzt ist "RUN CART" ausgewählt.
Drücke wieder die `ENTER`-Taste.
Splore lädt jetzt das "Spiel" aus dem Internet herunter.
Aber noch ist es kein Spiel.
Es sind nur die benötigten Ressourcen.
Auf dem Bildschirm siehst du jetzt folgendes:

![Screen-Shot des Space Game Tutoriums 01](images/02-space-game/space-game-tut-01.png "Space game tut 01")

Das Spiel musst du jetzt schreiben.

Dafür drücke die `ESC`-Taste.
Auf dem folgenden Bildschirm wählst du "EXIT TO SPLORE".
Damit bist du aus dem Spiel heraus und wieder zurück in Splore.

![Screen-Shot zurück zu Splore](images/02-space-game/exit-to-splore.png "Exit to Splore")

Jetzt musst du wieder die `ESC`-Taste drücken.
Damit gelangst du aus Splore auf die Pico-8 Konsole.
Die Konsole erkennst du an dem `>`-Zeichen am Anfang der Zeile.
Hier kannst du Befehle eingeben.
Mit `run` zum Beispiel kannst du Spiele starten.

![Screen-Shot zurück zum Editor](images/02-space-game/exit-to-editor.png "Exit to editor")

Und noch einmal must du die `ESC`-Taste drücken.
Mit `ESC` wechselst du zwischen Konsole und Editor hin und her.
Im Code-Editor siehst du jetzt das kleine Programm, welches ich vorbereitet habe.

![Screen-Shot Code-Editor](images/02-space-game/code-editor.png "Code editor")

Wenn du rechts oben auf das Symbol für den Sprite/Tile-Editor klickst, kannst du die Grafoksymbolse sehen, die ich vorbereitet habe.

![Screen-Shot Sprite-Editor](images/02-space-game/sprite-editor.png "Sprite editor")

## Die Pico-8 Hauptschleife

![expains](images/computer_asks.png)
Was denkst du macht der folgende Code?
Wofür könnten die Funktionen `_init`, `_update60` und `_draw` stehen?
(Es ist nicht schlimm, wenn du nicht drauf kommst.
Ich erkläre es gleich.)

```lua
--space-tut-01
--hzahnleiter
_init = function()
end

_update60 = function()
end

_draw = function()
  cls()
  print("space",44,40,2)
  print("game",48,50,3)
  print("tutorial",32,60,4)
  print("part 01",64,70,9)
  spr(1,60,120)
end
```

![expains](images/computer_explains.png)
Wie du gesehen hast, kann Pico-8 beliebige Programme ausführen.
Aber, wenn die Funktionen `_init`, `_update60` und `_draw` definiert sind, dann passiert folgendes:

*  `_init`: Die Funktion wird einmal ganz am Anfang aufgerufen.
        Das ist der Platz, um das Spiel zu **initialisieren**.
        Das bedeutet, dass hier z.B. wichtige Variablen definiert werden usw.
*  `_update60`: Die Funktion wird 60 mal pro Sekunde aufgerufen.
        Das ist der Platz, um den Zustand deines Spiels neu zu berechnen und zu aktualisieren.
        Das bedeutet zum Beispiel, dass du hier die Steuerungstasten einlesen kanns.
        Hier schiebst du auch die Spielfigur weiter.
        Du wirst gleich sehen, wie das geht.
*  `_draw`: Nachdem zum Beispiel die Positionen der Spieler in `_update60` aktuaisiert wurden, ist das der Platz wo du den Programmcode unter bringst, der das Bild tatsächlich neu zeichnet.

![expains](images/computer_asks.png)
Warum wird `_update60` 60 mal pro Sekunde aufgerufen?
Du hast vielleicht schon mal das Wort "Frame Rate" oder "Frames per Second" gehört?
Zu Deutsch "Bildwiederholrate".
Klassischerweise zeichnet man in Deutschland einen Computerbildschirm 50 mal pro Sekunde neu.
Das ist einfach durch die alte PAL-Norm vorgegeben.
In den USA zeichnet man klassischerweise den Computerbildschirm 60 mal pro Sekunde neue.
In den USA wird die alte NTSC-Norm verwendet.
Auch wenn die Normen schon sehr alt sind, so haben sich doch einige Dinge erhalten.
50Hz oder 60Hz sind genug Bilder pro Sekunde, um uns Menschen ein flüssiges Bild vorzutäuschen.

Hast du bemerkt: Im Unterschied zu den Programmen aus der Einführung, kommt jetzt kein Prompt mehr.
Das Prompt, auf Deutsch "Eingabeaufforderung", ist das `>`-Zeichen.
Dahinter blinkt der sogenannte Cursor.
Hier kannst du deine Eingaben machen.
Du "Bild" beleibt stehen.
Das liegt daran, dass Pico-8 die `_draw`-Funktion in einer Endlosschleife ausführt.
Du must erst `ESC` drücken.
Dann beendet Pico-8 die Schleife und du bist wieder auf der Konsole.

Hier noch mal der Ablauf als Diagramm:

```mermaid
graph TD
  _init([_init])
  _update60([_update60])
  _draw([_draw])
  KONSOLE[/Konsole/]

  _init --> _update60
  _init --ESC--> KONSOLE
  _update60 --> _draw
  _update60 --ESC--> KONSOLE
  _draw --> _update60
  _draw --ESC--> KONSOLE
```

| [&#8592; voherige Übung](01-pico-8-intro-01.md) | [&#8593; zurück zur Übersicht &#8593;](../README.md) | [nächste Übung &#8594;](02-space-game-02.md) |