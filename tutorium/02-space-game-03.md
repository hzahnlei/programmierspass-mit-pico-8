# Space Game Tutorium (Fortsetzung)

## Jetzt fliegt das Raumschiff

So richtig fliegen tut unser Raumschiff aber noch nicht.
Gleich lernst du, wie du die Illusion erzeugst, dass das Raumschiff fliegen würde.

Tippe wieder folgendes ab.
(Oder kopiere es über die Zwischenablage.)
Ich erkläre dir gleich, was passiert.

```lua
_init = function()
  ship = {x=63, y=120, speed=2, sprite=1}

  stars={}
  star_cols={1,2,5,6,7,12}
  warp_factor=3
  for i = 1, #star_cols do
    for j = 1, 10 do
      local s={
        x=rnd(128),
        y=rnd(128),
        z=i,
        c=star_cols[i]
      }
      add(stars,s)
    end 
  end
end

_update60 = function()
  if (btn(0)) then
    ship.x -= ship.speed
    if ship.x<0 then ship.x=0 end
  elseif (btn(1)) then
    ship.x += ship.speed
    if ship.x>121 then ship.x=121 end
  end

  for s in all(stars) do
    s.y += s.z * warp_factor / 10
    if s.y > 128 then
      s.y = 0
      s.x = rnd(128)
    end
  end
end

_draw = function()
  cls()

  for s in all(stars) do
    pset(s.x,s.y,s.c)
  end

  spr(ship.sprite, ship.x, ship.y)
end
```

Jetzt führe das Programm aus.

![starfield](images/02-space-game/starfield.gif)

![excited](images/computer_is_excited.png)
Na! Ist das nicht ein Super Effekt?

Das Sternenfeld habe ich von einem [anderen Tutorium](https://forum.clockworkpi.com/t/pico-8-gamedev-2-stars-space-tutorial/2455) übernommen und nur geringfügig geändert.

## Organisation ist alles!

![excited](images/computer_explains.png)
Bevor ich dir erkläre, wie das mit den Sternen funktioniert, habe ich hier eine wichtige Botschaft für dich.
Du musst im Programm Ordnung halten.
Die siehst, das Programme sehr schnell sehr groß werden.
Wenn man seinen Programmcode nicht ordentlich organisiert und strukturiert, dann ist man schnell verloren.

Was heißt _"den Code organisieren"_?
Zum Beispiel kann man verschiedene Codestücke in eigene Funktionen auslagern.
Man kann eigene Funktionen für das Raumschiff schreiben.
Und wiederum eigene Funktionen für das Sternenfeld.
Damit hält man die verschiedenen Aspekte des Programms besser auseinander.
Ich zeige es dir.

```lua
_init = function()
  init_ship();
  init_starfield()
end

init_ship = function()
  ship = {x=63, y=120, speed=2, sprite=1}
end

init_starfield = function()
  starfield = {stars={}, star_cols={1,2,5,6,7,12}, warp_factor=3}
  for i = 1, #starfield.star_cols do
    for j = 1, 10 do
      local s={
        x=rnd(128),
        y=rnd(128),
        z=i,
        c=starfield.star_cols[i]
      }
      add(starfield.stars,s)
    end 
  end
end

_update60 = function()
  update_ship()
  update_starfield()
end

update_ship = function()
  if (btn(0)) then
    ship.x -= ship.speed
    if ship.x<0 then ship.x=0 end
  elseif (btn(1)) then
    ship.x += ship.speed
    if ship.x>121 then ship.x=121 end
  end
end

update_starfield = function()
  for s in all(starfield.stars) do
    s.y += s.z * starfield.warp_factor / 10
    if s.y > 128 then
      s.y = 0
      s.x = rnd(128)
    end
  end
end

_draw = function()
  cls()
  draw_starfield()
  draw_ship()
end

draw_starfield = function()
  for s in all(starfield.stars) do
    pset(s.x,s.y,s.c)
  end
end

draw_ship = function()
  spr(ship.sprite, ship.x, ship.y)
end
```

## Und wie funktioniert das jetzt mit den Sternen?

![asks](images/computer_asks.png)
Und wie funktioniert das jetzt mit den Sternen?

![explains](images/computer_explains.png)
Pass gut auf.
Das ist jetzt schon etwas anspruchsvoller.

### _init

```lua
_init = function()
  init_ship();
  init_starfield()
end
```

Die Funktion ist jetzt sehr viel kürzer geworden.
Denn wir haben die Initialisierung von Raumschiff und Sternenfeld in eigene Funktionen ausgelagert.

### init_ship

```lua
init_ship = function()
  ship = {x=63, y=120, speed=2, sprite=1}
end
```

Wie gehabt.
Unter dem Namen `ship` fassen wir folgende Daten zusammen: X- und Y-Koordinate des Schiffs, Geschwindigkeit und Sprite (Form).

### init_starfield

```lua
init_starfield = function()
  starfield = {stars={}, star_cols={1,2,5,6,7,12}, warp_factor=3}
  for i = 1, #starfield.star_cols do
    for j = 1, 10 do
      local s={
        x=rnd(128),
        y=rnd(128),
        z=i,
        c=starfield.star_cols[i]
      }
      add(starfield.stars,s)
    end 
  end
end
```

Unter dem Namen `starfield` fassen wir alle Daten zusammen, die mit dem Sternenfeld zu tun haben. `stars` ist eine Liste von Sternen (kleine Punkte auf dem Bildschirm).
`star_cols` ist eine Liste von Farben, die wir für die Sterne benutzen wollen.
Und `warp_factor` ist die maximale Geschwindigkeit, mit der die Sterne an uns vorüber ziehen.

Die Liste der Sterne `starfield.stars` ist zunächst leer.
In der `for`-Schleife wird die Liste mit sternen befüllt (`add(starfield.stars,s)`).
Für von jeder Farbe werden zehn Sterne erzeugt.

Und noch etwas ist besonders interessant.
Die X- und Y-Positionen der Sterne sind zufällig generiert.
Die `rnd`-Funktion kennst du ja schon.
Aber, jeder Stern `s` hat auch eine Z-Position.
**Was ist das denn schon wieder?**
Die Z-Position gibt die Tiefe an.
Die Sterne mit der kleinsten Tiefe erhalten die dunkelste Farbe und bewegen sich am langsamsten.
Die Sterne mit der größten Tiefe erhalten die hellste Farbe und bewegen sich am schnellsten.
Damit erhält das Sternenfeld eine scheinbare Tiefe.
Diesen tollen Trick nennt man **Parallaxeneffekt**.

### _update60

```lua
_update60 = function()
  update_ship()
  update_starfield()
end
```

Auch diese Funktion ist geschrumpft.
Die Steuerung des Raumschiffs und die Bewegung der Sterne wurden ausgelagert.

### update_ship

```lua
update_ship = function()
  if (btn(0)) then
    ship.x -= ship.speed
    if ship.x<0 then ship.x=0 end
  elseif (btn(1)) then
    ship.x += ship.speed
    if ship.x>121 then ship.x=121 end
  end
end
```

Hier hat sich nichts geändert, außer, dass der Code jetzt in einer eigenen Funktion steckt.

### update_starfield

```lua
update_starfield = function()
  for s in all(starfield.stars) do
    s.y += s.z * starfield.warp_factor / 10
    if s.y > 128 then
      s.y = 0
      s.x = rnd(128)
    end
  end
end
```

Das ist jetzt etwas komplizierter.
`for s in all(starfield.stars) do` nimmt sich jetzt jeden Stern einzeln vor.
Er wird gemäß seiner Tiefe (`z`) auf der Y-Achse von oben nach unten bewegt.

**Achtung!**
Was passiert denn, wenn der Stern unten angekommen ist?
Dann wird er wieder ganz nach oben gesetzt (`s.y = 0`).
Seine neue X-Position wird zufällig gewählt (`rnd(128)`).

### _draw

```lua
_draw = function()
  cls()
  draw_starfield()
  draw_ship()
end
```

Auch diese Funktion ist wieder auf Zwergengröße geschrumpft.

![asks](images/computer_asks.png)
Weißt du, warum wir zuerst die Sterne und dann das Schiff darstellen?

![explains](images/computer_explains.png)
Die Sterne stellen den hintergrund dar.
Sie sollen auf keinen Fall über das Raumschiff drüber gemalt werden.

### draw_starfield

```lua
draw_starfield = function()
  for s in all(starfield.stars) do
    pset(s.x,s.y,s.c)
  end
end
```

Mit `for s in all(starfield.stars) do` wird wieder jeder einzelen Stern besucht.
Mit `pset(s.x,s.y,s.c)` wird dann ein kleiner Punkt auf dem Bildschirm gemalt.
`pset` ist eine neue Funktion, die du noch nicht kennst.


### draw_ship

```lua
draw_ship = function()
  spr(ship.sprite, ship.x, ship.y)
end
```

Die Funktion kennst du schon.
Sie ist einfach und zeichnet nur das Bild des Raumschiffs (`ship.sprite`) and die richtige Stelle (`ship.x` und `ship.y`).

Uff!
Das waren eine Menge neuer Dinge zum Lernen.
Spiele noch ein wenig mit dem Programm herum, dann mache eine Pause.

Was passiert denn zum Beispiel, wenn du das `cls()` aus der `_draw` Funktion entfernst?

| [&#8592; voherige Übung](02-space-game-02.md) | [&#8593; zurück zur Übersicht &#8593;](../README.md) | [nächste Übung &#8594;](02-space-game-04.md) |